﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CRM.Data.Context;
using CRM.Data.Models;
using CRM.Data.Repositories.Interfaces;
using System.Web.Http.Cors;

namespace CRM.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OfficesController : ApiController
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly IOfficeRepository _officeRepository;
        public OfficesController
            (
                IOfficeRepository officeRepository,
                IBookingRepository bookingRepository
            )
        {
            _bookingRepository = bookingRepository;
            _officeRepository = officeRepository;
        }
        private DbBusinessContext db = new DbBusinessContext();

        // GET: api/Offices

        public List<OfficeAndFirstBookingModel> GetOffices()
        {
            List<OfficeAndFirstBookingModel> list = new List<OfficeAndFirstBookingModel>();

            var allOffice = _officeRepository.SelectAll();
            if (_bookingRepository.Count() != 0)
            {
                foreach (var current in allOffice)
                {
                    if (current.Booking.Count() != 0)
                    {
                        var newBookings = current.Booking.FindAll(p => p.StartDate.CompareTo(DateTime.Now) == 1).Count();
                        if (newBookings != 0)
                        {
                            
                            
                            var firstBooking = current.Booking.OrderBy(p => p.StartDate).OrderByDescending(p => p.BookingStatus).Where(p => p.StartDate.CompareTo(DateTime.Now) == 1).First();
                            if (firstBooking.BookingStatus != Status.New)
                            {
                                list.Add(new OfficeAndFirstBookingModel { Office = current, StartDate = firstBooking.StartDate.ToString(), StopDate = firstBooking.StopDate.ToString() });
                            }
                            else
                            {
                                list.Add(new OfficeAndFirstBookingModel { Office = current });
                            }

                        }
                        else
                        {
                            list.Add(new OfficeAndFirstBookingModel { Office = current });
                        }
                    }
                    else
                    {
                        list.Add(new OfficeAndFirstBookingModel { Office = current });
                    }
                }
            }
            else
            {
                foreach (var item in allOffice)
                {
                    list.Add(new OfficeAndFirstBookingModel { Office = item });
                }
            }


            return list;
        }
        //GET: api/Offices/5

        public Office GetOffice(Guid id)
        {
            Office office = _officeRepository.SelectByID(id);
            
            return office;
        }
        //GET: api/Offices/5

        public List<Booking> GetAcceptedBookings(Guid id)
        {
            Office office = _officeRepository.SelectByID(id);
            var bookingAccepted =  office.Booking.FindAll(p => p.BookingStatus == Status.Accepted);
            return bookingAccepted;
        }

        // PUT: api/Offices/5
        //[ResponseType(typeof(void))]
        [HttpPut]
        public void PutOffice(Guid id, [FromBody] Office office)
        {
            var _office = _officeRepository.SelectByID(id);
            _office.Name = office.Name;
            _office.Chairs = office.Chairs;
            _office.Projector = office.Projector;
            _office.MarkerBoards = office.MarkerBoards;
            _officeRepository.Update(_office);
        }

        //POST: api/Offices
        [Route("api/offices/PostOffice")]
        [HttpPost]
        public void PostOffice([FromBody] Office office)
        {
            _officeRepository.Add(office);
            _officeRepository.Save();

        }

        // DELETE: api/Offices/5
        //[ResponseType(typeof(Office))]
        public void DeleteOffice(Guid id)
        {
            var office = _officeRepository.SelectByID(id);
            _officeRepository.Delete(office);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}