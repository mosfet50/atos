﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CRM.Data.Context;
using CRM.Data.Models;
using CRM.Data.Repositories.Interfaces;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using System.Globalization;
using System.Threading;

namespace CRM.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BookingsController : ApiController
    {
        private readonly IBookingRepository _bookingRepository;
        public BookingsController(IBookingRepository bookingRepository)
        {
            _bookingRepository = bookingRepository;
        }

        private DbBusinessContext db = new DbBusinessContext();

        // GET: api/Bookings
        [HttpGet]
        public IEnumerable<Booking> GetBookings()
        {
            var bookings = _bookingRepository.FindAll(p => p.BookingStatus == Status.New);
            return bookings;
        }

        [HttpPut]
        public void AcceptBooking(Guid Id)
        {
            var booking = _bookingRepository.SelectByID(Id);
            booking.BookingStatus = Status.Accepted;
            _bookingRepository.Update(booking);
        }
        

        [System.Web.Http.Route("api/bookings/AddBooking")]
        [System.Web.Http.HttpPost]
        public string AddBooking(BookingModel booking)
        {
            var strInfo = "Null";
            var chosenTime = DateTime.Compare(Convert.ToDateTime(booking.StartDate), DateTime.Now);
            var dateTime = booking.StopDate.CompareTo(booking.StartDate);
            if (chosenTime<1)
            {
                strInfo = "Время не актуально!!!";
                return strInfo;
            }
            if (dateTime < 1)
            {
                strInfo = "Время начала мероприятия позже времени окончания мероприятия";
                return strInfo;
            }
            if (chosenTime == 1)
            {
                foreach (var current in _bookingRepository.FindAll(p=> p.BookingStatus == Status.Accepted && p.IdOffice == booking.IdOffice))
                {
                    var StartDate = current.StartDate.ToString();
                    var StopDate = current.StopDate.ToString();

                    int startIntevalStart = DateTime.Compare(current.StartDate, Convert.ToDateTime(booking.StartDate));
                    int startIntevalStop = DateTime.Compare(current.StopDate, Convert.ToDateTime(booking.StartDate));
                    int stopIntervalStart = DateTime.Compare(current.StartDate, Convert.ToDateTime(booking.StopDate));
                    int stopIntervalStop = DateTime.Compare(current.StopDate, Convert.ToDateTime(booking.StopDate));
                    if (startIntevalStart == 0 && stopIntervalStop == 0)
                    {
                        strInfo = "Время занято!";
                        return strInfo;
                    }
                    else if (startIntevalStart == -1 && startIntevalStop == 1)
                    {
                        strInfo = "Время занято!";
                        return strInfo;
                    }
                    else if (stopIntervalStart == -1 && stopIntervalStop == 1)
                    {
                        strInfo = "Время занято!";
                        return strInfo;
                    }
                    else if (startIntevalStart == 1 && stopIntervalStop == -1 )
                    {
                        strInfo = "Время полностью перекрывает другое время!";
                        return strInfo;
                    }
                }
                Booking _booking = new Booking();
                _booking.StartDate = Convert.ToDateTime(booking.StartDate);
                _booking.StopDate = Convert.ToDateTime(booking.StopDate);
                _booking.IdOffice = booking.IdOffice;
                _booking.EventName = booking.EventName;
                _bookingRepository.Add(_booking);
                _bookingRepository.Save();
                strInfo = "OK";
            }
            return strInfo;
        }

        [System.Web.Http.Route("api/bookings/PostSubmitdata")]
        [System.Web.Http.HttpPost]
        public string PostSubmitdata(Booking emp)
        {
            return emp.StartDate.ToString();
        }

        // DELETE: api/Bookings/5
        [HttpDelete]
        public void DeleteBooking(Guid id)
        {
            var booking = _bookingRepository.SelectByID(id);
            _bookingRepository.Delete(booking);
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}