﻿using CRM.Data.Context;
using CRM.Data.Models;
using CRM.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        DbBusinessContext db = new DbBusinessContext();
        private readonly IOfficeRepository _officeRepository;


        public HomeController
            (
                IOfficeRepository officeRepository

            )
        {
            _officeRepository = officeRepository;

        }
        [Route("/")]
        public ActionResult Index()
        {
            var offices = _officeRepository.SelectAll()
                .OrderBy(x => x.Name)
                .ThenBy(x => x.Chairs)
                .ThenBy(x => x.Projector)
                .ThenBy(x => x.MarkerBoards).ToList();
            return View(offices);
        }
        public ActionResult Client()
        {
            return View();
        }

            public ActionResult Minor()
        {
            ViewData["SubTitle"] = "Simple example of second view";
            ViewData["Message"] = "Data are passing to view by ViewData from controller";
            return View();
        }
        public ActionResult OfficeList()
        {
            var offices = _officeRepository.SelectAll()
                .OrderBy(x => x.Name)
                .ThenBy(x => x.Chairs)
                .ThenBy(x => x.Projector)
                .ThenBy(x => x.MarkerBoards).ToList();
            return View(offices);
        }
    }
}