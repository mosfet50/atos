﻿using Autofac;
using Autofac.Integration.Mvc;
using CRM.Data.Context;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Owin;
using System.Web;
using System.Web.Mvc;
using CRM.Data.Repositories.Interfaces.BusinessUsers;
using CRM.Data.Repositories.BusinessUsers;
using CRM.Data.Models;
using CRM.Data.Repositories.Interfaces;
using CRM.Data.Repositories;
using System.Web.Compilation;
using System.Reflection;
using System.Linq;
using Autofac.Integration.WebApi;
using System.Web.Http;
using CRM.App_Start;

[assembly: OwinStartupAttribute(typeof(CRM.Startup))]
namespace CRM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            // REGISTER DEPENDENCIES
            builder.RegisterType<DbIdentityContext>().AsSelf().InstancePerRequest();
            builder.RegisterType<DbBusinessContext>().AsSelf().InstancePerRequest();
            builder.RegisterType<AspNetUserStore>().As<IUserStore<AspNetUser>>().InstancePerRequest();
            builder.RegisterType<AspNetUserManager>().AsSelf().InstancePerRequest();
            builder.Register<IAuthenticationManager>(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
            builder.Register<IDataProtectionProvider>(c => app.GetDataProtectionProvider()).InstancePerRequest();


            //CUSTOM DI
            //builder.RegisterType(typeof(AspNetUserTypeRepository)).As(typeof(IBusinessUserTypeRepository)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(OfficeRepository)).As(typeof(IOfficeRepository)).InstancePerLifetimeScope();
            builder.RegisterType(typeof(BookingRepository)).As(typeof(IBookingRepository)).InstancePerLifetimeScope();

            var assemblies = BuildManager.GetReferencedAssemblies().Cast<Assembly>().ToArray();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            // REGISTER CONTROLLERS SO DEPENDENCIES ARE CONSTRUCTOR INJECTED
            builder.RegisterControllers(typeof(MvcApplication).Assembly);





            // BUILD THE CONTAINER
            var container = builder.Build();

            // REPLACE THE MVC DEPENDENCY RESOLVER WITH AUTOFAC
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // REGISTER WITH OWIN
            HttpConfiguration config = new HttpConfiguration();
            //// register api routing.
            WebApiConfig.Register(config);
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);

            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();

            ConfigureAuth(app);
        }
    }
}
