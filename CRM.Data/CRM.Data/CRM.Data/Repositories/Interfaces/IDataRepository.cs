﻿using CRM.Data.GenericRepo;
using CRM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.Repositories.Interfaces.BusinessUsers
{
    public interface IDataRepository : IGenericRepository<Models.Data>
    {
    }
}
