﻿using SharpRepository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.GenericRepo
{
    public interface IGenericRepository<T> : IRepository<T, Guid> where T : class
    {
        T SelectByID(Guid id);
        IEnumerable<T> SelectAll();
        void Save();
    }
}
