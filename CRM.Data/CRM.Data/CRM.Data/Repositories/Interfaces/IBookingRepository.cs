﻿using CRM.Data.GenericRepo;
using CRM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.Repositories.Interfaces
{
    public interface IBookingRepository : IGenericRepository<Booking>
    {
    }
}
