﻿using CRM.Data.Context;
using CRM.Data.GenericRepo;
using CRM.Data.Models;
using CRM.Data.Repositories.Interfaces.BusinessUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.Repositories.BusinessUsers
{
    public class DataRepository : GenericRepository<Models.Data, Guid>, IDataRepository
    {
        public DataRepository(DbBusinessContext Context) : base(Context)
        {
        }
    }
}
