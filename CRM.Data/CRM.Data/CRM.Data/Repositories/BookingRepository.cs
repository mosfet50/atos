﻿using CRM.Data.GenericRepo;
using CRM.Data.Repositories.Interfaces;
using System;
using CRM.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Data.Context;

namespace CRM.Data.Repositories
{
    public class BookingRepository : GenericRepository<Booking, Guid>, IBookingRepository
    {
        public BookingRepository(DbBusinessContext Context) : base(Context)
        { }
    }
}
