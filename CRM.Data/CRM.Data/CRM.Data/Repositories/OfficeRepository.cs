﻿using CRM.Data.Context;
using CRM.Data.GenericRepo;
using CRM.Data.Models;
using CRM.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.Repositories
{
    public class OfficeRepository : GenericRepository<Office, Guid>, IOfficeRepository
    {
        public OfficeRepository(DbBusinessContext Context) : base(Context)
        { }
    }
}
