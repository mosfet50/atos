﻿using CRM.Data.Context;
using SharpRepository.EfRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.GenericRepo
{
    public class GenericRepository<T, TKey> : EfRepository<T, TKey> where T : class
    {

        internal GenericRepository(DbBusinessContext dbContext) : base(dbContext)
        {

        }

        protected override void AddItem(T entity)
        {
            if (typeof(TKey) == typeof(Guid) || typeof(TKey) == typeof(string))
            {
                TKey id;
                if (GetPrimaryKey(entity, out id) && Equals(id, default(TKey)))
                {
                    id = GeneratePrimaryKey();
                    SetPrimaryKey(entity, id);
                }
            }
            DbSet.Add(entity);
        }

        public IEnumerable<T> SelectAll()
        {
            return GetAll();
        }

        public T SelectByID(TKey id)
        {
            return Get(id);
        }

        public void Save()
        {
            SaveChanges();
        }

        protected override void SaveChanges()
        {
            Context.SaveChanges();
        }

        private TKey GeneratePrimaryKey()
        {
            if (typeof(TKey) == typeof(Guid))
            {
                return (TKey)Convert.ChangeType(Guid.NewGuid(), typeof(TKey));
            }

            if (typeof(TKey) == typeof(string))
            {
                return (TKey)Convert.ChangeType(Guid.NewGuid().ToString(), typeof(TKey));
            }

            throw new InvalidOperationException("Primary key could not be generated. This only works for GUID, Int32 and String.");
        }
    }
}
