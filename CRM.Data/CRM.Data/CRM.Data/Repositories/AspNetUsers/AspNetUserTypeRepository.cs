﻿using CRM.Data.Context;
using CRM.Data.GenericRepo;
using CRM.Data.Models;
using CRM.Data.Repositories.Interfaces.BusinessUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.Repositories.BusinessUsers
{
    public class AspNetUserTypeRepository : GenericRepository<Models.Data, Guid>, IDataRepository
    {
    
        public AspNetUserTypeRepository(DbBusinessContext Context) : base(Context)
        {
        }
    }
}
