﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Data.Models;

namespace CRM.Data.Context
{
    public class DbIdentityContext : IdentityDbContext<AspNetUser>
    {

        public DbIdentityContext()
        : base("ConnectionStrings")
        {

        }
    }

    public class DbBusinessContext : DbContext
    {
        public DbBusinessContext()
        : base("ConnectionStrings")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Office>().ToTable("Offices");
            base.OnModelCreating(modelBuilder);
        }

        public System.Data.Entity.DbSet<CRM.Data.Models.Office> Offices { get; set; }

        public System.Data.Entity.DbSet<CRM.Data.Models.Booking> Bookings { get; set; }
    }

    public class AspNetUserStore : UserStore<AspNetUser>
    {
        public AspNetUserStore(DbIdentityContext context)
            : base(context)
        {
        }
    }

    public class AspNetUserManager : UserManager<AspNetUser>
    {
        public AspNetUserManager(IUserStore<AspNetUser> store)
        : base(store)
        {

        }

    }


}
