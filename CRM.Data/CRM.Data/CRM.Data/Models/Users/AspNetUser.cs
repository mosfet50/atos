﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CRM.Data.Models
{
    public class AspNetUser : IdentityUser
    {

        //public virtual ICollection<Data> UserCreated { get; set; }
        //public virtual ICollection<Data> UserLastEdited { get; set; }
        public virtual ICollection<Data> Data { get; set; }
    }
}
