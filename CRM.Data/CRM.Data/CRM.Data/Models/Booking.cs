﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.Models
{
    public class Booking
    {

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string EventName { get; set; }
        public Guid IdOffice { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StopDate { get; set; }
        public Status BookingStatus { get; set; }


        [ForeignKey("IdOffice")]
        public virtual Office Office { get; set; }
    }

    public enum Status
    {
        New,
        Accepted,
        Rejected
    }
}
