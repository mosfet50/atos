﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.Models
{
  
    public class OfficeAndFirstBookingModel
    {
      
        public Office Office { get; set; } 
        public string StartDate { get; set; }
        public string StopDate { get; set; }
    }
}
