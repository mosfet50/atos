﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.Models
{
    public class Office
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Chairs { get; set; }
        public bool Projector { get; set; }
        public bool MarkerBoards { get; set; }

        public virtual List<Booking> Booking { get; set; }
    }
}
