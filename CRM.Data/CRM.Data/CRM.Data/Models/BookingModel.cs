﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.Data.Models
{
    public class BookingModel
    {
        public string EventName { get; set; }
        public Guid IdOffice { get; set; }
        public string StartDate { get; set; }
        public string StopDate { get; set; }
        public Status BookingStatus { get; set; }
    }
}
