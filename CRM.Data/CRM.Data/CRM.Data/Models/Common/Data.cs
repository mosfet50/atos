﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRM.Data.Models
{
    public class Data
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public DateTime DateCreationObject { get; set; }
        public DateTime DateLastEdited { get; set; }
        public string UserIdCreated { get; set; }
        public string UserIdLastEdited { get; set; }

        [ForeignKey("UserIdCreated")]
        public virtual AspNetUser UserCreated { get; set; }
        [ForeignKey("UserIdLastEdited")]
        public virtual AspNetUser UserLastEdited { get; set; }

    }
}
